#include<reg52.h>
#include <intrins.h>
#define uint unsigned int
#define uchar unsigned char
char a,miao,shi,fen,ri,yue,nian,keynum;
int temp;/*时间分段标志*///,year1,month1,day1;

#define h1 0x80 //LCD第一行的初始化位置
#define h2 0x80+0x40 //LCD第二行初始化位置

//定义1602相关管脚
sbit rs=P1^2;
sbit en=P1^0;
sbit rw=P1^1;

//DS1302芯片的管脚定义
sbit DSIO=P1^5;
sbit SCLK=P1^4;
sbit RST=P1^6;

sbit ACC0=ACC^0;//设置累加器
sbit ACC7=ACC^7;

//按键
sbit key1=P3^2;
sbit key2=P3^3;
sbit key3=P3^4;

void delay2(uint s)//延时，用于温度程序部分
{
	while(s--);//区分i，用s表示
}

void delay(uint z)//延时函数
{
	uint x,y;
	for(x=z;x>0;x--)
	for(y=110;y>0;y--);
}
void writecom(uchar com)//写入指令函数
{	
	rs=0;
	rw=0;
	P0=com;
	delay2(1);
	en=1;
	delay2(1);
	en=0;
}
void writedata(uchar dat)//写入数据函数
{
	rs=1;
	rw=0; 
	P0=dat;
	delay2(1);
	en=1;
	delay2(1);
	en=0;
}
void print(uchar a3,uchar *str)//写字符串函数（没有延时）
{
	writecom(a3|0x80);
	while(*str!='\0')
	{
		//delay(100);//延时一下
		writedata(*str++);
	}
	*str=0;
}

void lcdinit()//1602初始化函数
{
	writecom(0x38);//设置为两行显示，8位显示
	writecom(0x0c);//开显示，不显示光标
	writecom(0x06);//光标右移
	writecom(0x01);//清屏		
}

/***************DS1302有关子函数********************/

//----------------------------------------
void write_1302(uchar addr, uchar dat)//向1302芯片写函数，指定写入地址，数据
{
    uchar n;
	RST = 0;
	_nop_();

	SCLK = 0;//先将SCLK置低电平。
	_nop_();
	RST = 1; //然后将RST(CE)置高电平。
	_nop_();

	for (n=0; n<8; n++)//开始传送八位地址命令
	{
		DSIO = addr & 0x01;//数据从低位开始传送
		addr >>= 1;
		SCLK = 1;//数据在上升沿时，DS1302读取数据
		_nop_();
		SCLK = 0;
		_nop_();
	}
	for (n=0; n<8; n++)//写入8位数据
	{
		DSIO = dat & 0x01;
		dat >>= 1;
		SCLK = 1;//数据在上升沿时，DS1302读取数据
		_nop_();
		SCLK = 0;
		_nop_();	
	}	
		 
	RST = 0;//传送数据结束
	_nop_();
}
uchar read_1302(uchar addr )//从1302读数据函数，指定读取数据来源地址
{
	uchar n,dat,dat1;
	RST = 0;
	_nop_();

	SCLK = 0;//先将SCLK置低电平。
	_nop_();
	RST = 1;//然后将RST(CE)置高电平。
	_nop_();

	for(n=0; n<8; n++)//开始传送八位地址命令
	{
		DSIO = addr & 0x01;//数据从低位开始传送
		addr >>= 1;
		SCLK = 1;//数据在上升沿时，DS1302读取数据
		_nop_();
		SCLK = 0;//DS1302下降沿时，放置数据
		_nop_();
	}
	_nop_();
	for(n=0; n<8; n++)//读取8位数据
	{
		dat1 = DSIO;//从最低位开始接收
		dat = (dat>>1) | (dat1<<7);
		SCLK = 1;
		_nop_();
		SCLK = 0;//DS1302下降沿时，放置数据
		_nop_();
	}

	RST = 0;
	_nop_();	//以下为DS1302复位的稳定时间,必须的。
	SCLK = 1;
	_nop_();
	DSIO = 0;
	_nop_();
	DSIO = 1;
	_nop_();
	return dat; 
}

uchar turnBCD(uchar bcd)//BCD码转换为十进制函数
{
	return((bcd>>4)*10+(bcd&0x0F));
}
void ds1302_init()//1302时钟芯片初始化函数
{
	RST=0;
	SCLK=0;
	write_1302(0x8e,0x00);//允许写
	write_1302(0x8e,0x80);//打开保护
}
/*下面是相关数据的显示函数*/
//时分秒显示函数
void writetime(uchar add,uchar dat)//写入时分秒
{
	uchar gw,sw;
	gw=dat%10;//取得个位数
	sw=dat/10;//取得十位数
	writecom(h2+add);//第二行显示
	writedata(0x30+sw);//显示该数字
	writedata(0x30+gw);
}
//年月日显示函数
void writeday(uchar add,uchar dat)//写入年月日函数
{
	uchar gw,sw;
	gw=dat%10;//取得个位数字
	sw=dat/10;//取得十位数字
	writecom(h1+add);//在第一行显示
	writedata(0x30+sw);
	writedata(0x30+gw);//显示
}
//按键扫描函数
void keyscan()
{
	if(key1==0)//设置键按下
	{
		delay(5);//延时
		if(key1==0)
		{
			while(!key1);
			keynum++;
			if(keynum>=8)
			keynum=1;//返回
			switch(keynum)
			{
			case 1:TR0=0;//关闭定时器
				   writecom(h2+0x0b);//秒的位置
				   writecom(0x0f);//设置为光标闪烁
				   temp=(miao)/10*16+(miao)%10;//秒化为bcd码
				   write_1302(0x8e,0x00);
				   write_1302(0x80,0x80|temp);//秒数据写入
				   write_1302(0x8e,0x80);
				   break;
			case 2:writecom(h2+8);//分的位置
				   break;//不用再次设置为闪烁状态了
			case 3:writecom(h2+5);//时的位置
				   break;
			case 4:writecom(h1+0x0c);//日的位置
				   break;
			case 5:writecom(h1+0x09);//月的位置
				  break;
			case 6:writecom(h1+0x06);//年的位置
				  break;
			case 7:writecom(0x0c);//第8次，光标不闪烁
				  TR0=1;//重新打开定时器
				  temp=(miao)/10*16+(miao)%10;
				  write_1302(0x8e,0x00);
				  write_1302(0x80,0x00|temp);//写入秒
				  write_1302(0x8e,0x80);
				  break;
			}
		}
	}
	if(keynum!=0)//当设置键按下时才能操作
	{
		if(key2==0)//加键
		{
			delay(5);
			if(key2==0)
			{
				while(!key2);//按键松开
				switch(keynum)
				{
					case 1:miao++;//
						   if(miao>=60)	miao=0;
						   writetime(0x0a,miao);/*在十位的位置写入，因为为两位数，个位数自动再后面显示*/
						   temp=(miao)/10*16+(miao)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x80,temp);// 写入秒
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h2+0x0b);//液晶模式为写入后自动右移，在此返回原来位置
						   break;
					case 2:fen++;
						   if(fen>=60) fen=0;
						   writetime(0x07,fen);//在十位数位置开始写入
						   temp=(fen)/10*16+(fen)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x82,temp);//写入分
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h2+0x08);//返回个位数的位置
						   break;
					case 3:shi++;
						   if(shi>=24) shi=0;
						   writetime(0x04,shi);//在0位开始写入
						   temp=(shi)/10*16+(shi)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x84,temp);//写入时
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h2+0x05);//返回到个位位置
						   break;
					case 4:ri++;
						   if(ri>=32) ri=1;
						   writeday(0x0b,ri);//注意是在十位开始写入
						   temp=(ri)/10*16+(ri)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x86,temp);//写入日
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h1+0x0c);//返回个位数
						   break;
					case 5:yue++;
						   if(yue>=13) yue=1;
						   writeday(0x08,yue);//在十位开始写入
						   temp=(yue)/10*16+(yue)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x88,temp);//写入月
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h1+0x09);//返回个位位置 
						   break;
					case 6:nian++;
						   if(nian>=100) nian=0;
						   writeday(0x05,nian);//在第一行第三个字符开始写入
						   temp=(int)((nian)/10*16+(nian)%10);//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x8c,temp);//写入年
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h1+0x06);//返回个位位置
						   break;
				}		   

			}
		}
		//以下是减的函数
		if(key3==0)
		{
			delay(5);//消抖
			if(key3==0)
			{
				while(!key3);
				switch(keynum)
				{
					case 1:miao--;/*此处有疑问：无符号数据是否要修改*/
						   if(miao<0) miao=59;//减到-1返回59
						   writetime(0x0a,miao);//在十位数写入 
						   temp=(miao)/10*16+(miao)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写
						   write_1302(0x80,temp);//写入秒
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h2+0x0b);//返回个位位置
						   break;
					case 2:fen--;
						   if(fen<0) fen=59;
						   writetime(0x07,fen);//在十位数位置写入
						   temp=(fen)/10*16+(fen)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写入
						   write_1302(0x82,temp);//写入分
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h2+8);//返回个位数位置
						   break;
				    case 3:shi--;
						   if(shi<0) shi=23;
						   writetime(0x04,shi);//在0位开始写入
						   temp=(shi)/10*16+(shi)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写入
						   write_1302(0x84,temp);//写入时
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h2+0x05);//返回到个位位置
						   break;
					case 4:ri--;
						   if(ri<1) ri=31;
						   writeday(0x0b,ri);//在十位开始显示
						   temp=(ri)/10*16+(ri)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写入
						   write_1302(0x86,temp);//写入日
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h1+0x0c);//返回个位数
						   break;
					case 5:yue--;
						   if(yue<1) yue=12;
						   writeday(0x08,yue);//在十位数位置开始写入
						   temp=(yue)/10*16+(yue)%10;//转换为bcd码
						   write_1302(0x8e,0x00);//允许写入
						   write_1302(0x88,temp);//写入月
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h1+0x09);//返回到个位位置
						   break;
					case 6:nian--;
						   if(nian<0) nian=99;
						   writeday(0x05,nian);//第一行第三个字符开始写入
						   temp=(int)((nian)/10*16+(nian)%10);//转换为bcd码
						   write_1302(0x8e,0x00);//允许写入
						   write_1302(0x8c,temp);//写入年
						   write_1302(0x8e,0x80);//打开保护
						   writecom(h1+0x06);//返回在年的尾数位置
						   break;
				}
			}
		}
	}
}
//初始化的函数
void init()//定时器初始化函数
{
	TMOD=0x01;//设置为定时器0和1的工作方式1
	TH0=(65536-60000)/256;//10毫秒
	TL0=(65536-60000)%256;
	EA=1;
	ET0=1;//允许T0中断
	TR0=1;//启动中断
}

/*以下是主函数部分*/
void main()
{
	lcdinit();//初始化液晶函数
	ds1302_init();//DS1302时钟芯片初始化函数
	init();//定时器初始化函数
	while(1)
	{
		keyscan();//不断扫描按键函数
	}
}
void timer0() interrupt 1//中断任务：取数据并显示
{
	TH0=(65536-60000)/256;//重新赋初值
	TL0=(65536-60000)%256;
//	TR0=0;
	//读取数据
	miao=turnBCD(read_1302(0x81));//读出秒
	fen=turnBCD(read_1302(0x83));//读出分
	shi=turnBCD(read_1302(0x85));//读出时
	ri=turnBCD(read_1302(0x87));//读出日
	yue=turnBCD(read_1302(0x89));//读出月
	nian=turnBCD(read_1302(0x8d));//读出年
	//显示数据
									
		print(0x80+3,"20");
		print(0x80+7,"/");
		print(0x80+10,"/");

		writeday(0x0b,ri);//显示日
		writeday(0x08,yue);//显示月
		writeday(0x05,nian);//显示年
		print(0x40+6,":");
		print(0x40+9,":");
		writetime(0x0a,miao);//显示出秒
		writetime(0x07,fen);//显示出分
		writetime(0x04,shi);//显示出时，第二行第一个开始
}